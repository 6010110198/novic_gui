import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.EventQueue;
import java.awt.TextField;

import javafx.scene.control.RadioButton;


public class Login extends JFrame {

    JFrame frame;
	JPasswordField passwordField;
	JTextField textField;
	//JRadioButton ch1, ch2;
	
	public Login() {
		frame = new JFrame();
		frame.setVisible(true);
		frame.setTitle("Login");
		

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
        JLabel title = new JLabel("****WELCOME TO NOVICE****");
        panel1.add(title);

		JLabel name = new JLabel("NAME :");
		panel2.add(name);

		textField = new JTextField(10);
		panel2.add(textField);

		JButton btn = new JButton("START");
		btn.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{  
				String name = textField.getText();
				Location lo = new Location(name);
				lo.setVisible(true);
				frame.setVisible(false);
			}  
		} 
	);
	
		panel3.add(btn);
		panel.add(panel1);
		panel.add(panel2);
		panel.add(panel3);
		frame.add(panel);
		frame.setSize(400, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}
	
}